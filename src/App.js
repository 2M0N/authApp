import {signInWithGoogle} from './firebase'
import './auth.css'

function App() {
  return (
    <>
    <button className='btn' 
    onClick={signInWithGoogle} > Sign in with Google </button>
    <h1> {localStorage.getItem('name')} </h1>
    <h2> {localStorage.getItem('email')} </h2>
    <img src={localStorage.getItem('dp')} alt="Profile Pic"/>
    </>
  );
}

export default App;
